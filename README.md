# Overview
This repository contains a POC for a Debezium server piping MSSQL change data capture (CDC) over to
rabbit MQ using MSSQL connector and rabbit MQ sink.

This demo is Debezium that is **NOT** backed by Kafka.

# Preparation

## Required Docker images
These images are required.

|Description|Command|
|--|--|
|Debezium Server|`docker pull debezium/server:2.3.4.Final`|
|MSSQL Server|`docker pull mcr.microsoft.com/mssql/server:2019-latest`|
|Rabbit MQ|`docker pull rabbitmq:3.12.6-management`|

## Docker Compose
There is a `docker-compose.yml` file at the root of this folder.

### Start MSSQL and RabbitMQ
Execute the following:
```
docker compose --profile setup up -d
```
This runs the `setup` profile for docker compose, that will start MSSQL and RabbitMQ

### Prepare the schema (MSSQL/Developer)
- Create a new folder named `mssql_data` and chmod it to 777.
    ```
    mkdir mssql_data
    chmod -R 777 mssql_data
    ```
- Look into the `mssql-init-scripts\setup.sql`.
- Refer to this SQL to **ENABLE CDC** for your own actual MSSQL database.
- Execute the SQL commands to prepare the DB and tables required for POC.
> You can connect to the MSSQL instance with the following settings
> - Host - `localhost:1443`
> - Username - `sa`
> - Password - `!Password1234`

### Prepare RabbitMQ
- Create a exchange of `topic` type named `demo.DebeziumDemoDB.dbo.DebeziumDemoTable`
- Rationale for this naming can be found here - https://debezium.io/documentation/reference/2.3/connectors/sqlserver.html#sqlserver-topic-names
- In a nutshell, it follows the convention of `<topicPrefix>.<schemaName>.<tableName>`.
    - For the above exchange name:
        - `demo` is the topic prefix (see `application.properties`).
        - `DebeziumDemoDB.dbo` is the schema name.
        - `DebeziumDemoTable` is the table name.
- Create a queue named `q.debezium.demo`.
- Bind the queue to the above exchange with a routing key the same as the queue name (for simplicity).
> Tip: The default credentials for RabbitMQ console can be found in the `docker-compose.yml` file under the `rabbitmq` service.

# Start Debezium
- Create a new folder named `debezium_resources/data` and chmod it to 777.
    ```
    mkdir debezium_resources/data
    chmod -R 777 debezium_resources/data
    ```
- Run the following
    ```
    docker compose --profile start up -d
    ```
- Watch the logs and ensure there are no errors.

# Test the CDC
Using the SQL statements provided in `setup.sql`, try simulating data changes to the table
and monitor the RabbitMQ's queue.

# Useful Links
Debezium Server - https://debezium.io/documentation/reference/stable/operations/debezium-server.html
Debezium Architecture - https://debezium.io/documentation/reference/stable/architecture.html
Advantages of CDC - https://debezium.io/blog/2018/07/19/advantages-of-log-based-change-data-capture/
Docker Hub for
- Debezium - https://hub.docker.com/r/debezium/server
- RabbitMQ - https://hub.docker.com/_/rabbitmq
- MSSQL - https://hub.docker.com/_/microsoft-mssql-server

# What's next?
- Figure out whether snapshots are required.
    - If the usage is just transient usage (i.e. only updates), snapshots may not be required.
    - But you may suffer lost updates if there is downtime to Debezium or RabbitMQ.
- Run it on actual environment after this POC.
- Try out Debezium Engine API - https://debezium.io/documentation/reference/2.3/development/engine.html
    - Debezium Engine API is supposedly the successor to Embedded Debezium.