USE master;

-- Create the DB
CREATE DATABASE DebeziumDemoDB;
USE DebeziumDemoDB;
-- Create the table
CREATE TABLE DebeziumDemoTable (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  status VARCHAR(255),
  last_modified_ts INTEGER
);

-- Enable CDC
EXEC sys.sp_cdc_enable_db;
-- Enable CDC on DB
ALTER DATABASE DebeziumDemoDB  
SET CHANGE_TRACKING = ON  
(CHANGE_RETENTION = 2 DAYS, AUTO_CLEANUP = ON)  ;
-- Verify CDC on DB
SELECT * 
FROM sys.change_tracking_databases 
WHERE database_id=DB_ID('DebeziumDemoDB');

-- Enable CDC on Table
EXEC sys.sp_cdc_enable_table
@source_schema = N'dbo',
@source_name   = N'DebeziumDemoTable', 
@role_name     = null,
@supports_net_changes = 1;





-- Verify CDC Jobs & access
EXEC sys.sp_cdc_help_change_data_capture;
SELECT database_id,name,is_cdc_enabled FROM sys.databases WHERE is_cdc_enabled = 1;
SELECT name,type,type_desc,is_tracked_by_cdc FROM sys.tables WHERE is_tracked_by_cdc = 1;
-- Verify that SQL Server Agent MUST be running
-- Look into /var/opt/mssql/log/sqlagent.out or any error logs for signs if agent is not starting
SELECT * FROM DebeziumDemoDB.sys.dm_server_services dss;

-- You can use these to check for any CDC errors.
select * from msdb.dbo.cdc_jobs;
EXEC msdb.dbo.sp_help_job;


-- You can use this to check for any CDC errors.
select * from sys.dm_cdc_errors;





-- Some useful SQL to simulate activity in the table.
insert into DebeziumDemoTable (id, name, status, last_modified_ts) values (1, 'ABC', 'pending', 1);
delete from DebeziumDemoTable where id > 0;
insert into DebeziumDemoTable (id, name, status, last_modified_ts) values (2, 'DEF', 'pending', 1);
update DebeziumDemoTable set last_modified_ts=3 where id=1;
select * from DebeziumDemoTable;